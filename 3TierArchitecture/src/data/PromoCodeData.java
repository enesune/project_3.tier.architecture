package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.LinkedList;
import java.util.List;

public class PromoCodeData implements IPromoCodeData {

	private List<String> promocodes;
	
	public PromoCodeData(){
		this.promocodes = new LinkedList<String>();
		this.loadPromoCodes();
	}
	
	@Override
	public int getPromoCodeCount() {
		return this.promocodes.size();
	}
	
	@Override
	public boolean savePromoCode(String code) {
		try {
			BufferedWriter bWrite = new BufferedWriter(new FileWriter("promoCodes.txt", true));
			bWrite.write(code + "\n");
			bWrite.flush(); //Sicherstellen dass der Promo Code in die Datei geschrieben wurde.
			bWrite.close(); //Datei schlie�en um sie �ffnen zu k�nnen.
		} catch (Exception e) {
			e.printStackTrace();
		}
		return this.promocodes.add(code);
	}
	
	public boolean loadPromoCodes() {
		try
		{
			BufferedReader bRead = new BufferedReader(new FileReader("promoCodes.txt"));
			String currLine = bRead.readLine();
			while (currLine != null) {
				this.promocodes.add(currLine);
				currLine = bRead.readLine();
			}
			bRead.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean isPromoCode(String code) {
		return this.promocodes.contains(code);
	}

}
