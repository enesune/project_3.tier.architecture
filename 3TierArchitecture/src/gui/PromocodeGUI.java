package gui;

//import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import logic.IPromoCodeLogic;
import javax.imageio.ImageIO;
import javax.print.DocFlavor.URL;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;

public class PromocodeGUI extends JFrame {

	private static final long serialVersionUID = 2303911575322895620L;
	private JPanel contentPane;
	private JLabel lblPromocode;
	int posX=0,posY=0;
	
	/**
	 * Create the frame.
	 */
	public PromocodeGUI(IPromoCodeLogic logic) {
		setAlwaysOnTop(true);
		setUndecorated(true);
		//setOpacity(0.9f);
		setTitle("PromotionCodeGenerator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 258);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(119, 136, 153));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		//draging this undecorated window
		addMouseListener(new MouseAdapter(){
		   public void mousePressed(MouseEvent e){
		      posX=e.getX();
		      posY=e.getY();
		   }
		});
		
		this.addMouseMotionListener(new MouseAdapter(){
		     public void mouseDragged(MouseEvent evt){
				//sets frame position when mouse dragged			
				setLocation (evt.getXOnScreen()-posX,evt.getYOnScreen()-posY);
			}
		});
		//end of this draging window thing
		//Quelle: https://www.codeproject.com/Tips/282494/Dragging-a-borderless-frame-in-Java
		
		
		lblPromocode = new JLabel("");
		lblPromocode.setHorizontalAlignment(SwingConstants.CENTER);
		lblPromocode.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		
		JButton btnNewPromoCode = new JButton("a genereted code is just a click away~");
		btnNewPromoCode.setFont(new Font("Showcard Gothic", Font.PLAIN, 14));
		btnNewPromoCode.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String code = logic.getNewPromoCode();
				lblPromocode.setText(code);
			}
		});
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(46, 139, 87));
		panel.setForeground(SystemColor.desktop);
		
		JButton btnClose = new JButton("");
		btnClose.setSelectedIcon(null);
		btnClose.setIcon(new ImageIcon(PromocodeGUI.class.getResource("/gui/close_button.png")));
		//btnClose.
		btnClose.setForeground(new Color(255, 0, 0));
		btnClose.setBackground(new Color(255, 0, 0));
		btnClose.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		
		JLabel lblNewLabel = new JLabel("UNIVERSAL KEYGEN ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Showcard Gothic", Font.PLAIN, 28));
		
		JComboBox<String> comboBox = new JComboBox<String>();
		comboBox.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 11));
		comboBox.addItem("Double your IT-$ per Lesson!");
		comboBox.addItem("Get 50 IT-$ at an instant!");
		comboBox.addItem("Let Mr.Tenbusch get sick!");
		comboBox.addItem("Wanna get 50 bucks from Mr. Tenbusch?");
		comboBox.addItem("Get a discount for IT-$ by exchanging from �");
		comboBox.addItem("Promocode to get the next exam");
		comboBox.addItem("Get 16 points in your testimonial");
		comboBox.addItem("Get invisible to drink cola in front of the teachers");
	
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addComponent(panel, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
					.addGap(16)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewPromoCode, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
						.addComponent(lblPromocode, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGap(25)
									.addComponent(comboBox, 0, 279, Short.MAX_VALUE))
								.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE))
							.addGap(18)
							.addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnClose, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
						.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 57, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addGap(63)
					.addComponent(lblPromocode, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNewPromoCode, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
		);
		contentPane.setLayout(gl_contentPane);
		this.setVisible(true);
	}
}